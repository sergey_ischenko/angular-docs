## Как в терминологии Angular соотносятся понятия "factory", "service", "provider"?

Всё это можно понять, внимательно прочитав вот эту страницу: https://docs.angularjs.org/api/auto/service/$provide.
Так что не удивляйтесь, что большинство ссылок будет вести на неё.

Также см: https://docs.angularjs.org/guide/providers.

---

**SERVICE**

[Service - это объект или примитив, который можно заинжектить (ниже - "зависимость")](https://docs.angularjs.org/guide/services):
`Angular services are substitutable objects that are wired together using dependency injection (DI)`
Фактически сервис - это просто способ отделить произвольный код в "модуль".

**SERVICE PROVIDER**

[Provider](https://docs.angularjs.org/api/auto/service/$provide#provider) - ЭТО ОБЪЕКТ ЛИБО ФУНКЦИЯ-КОНСТРУКТОР, СОЗДАЮЩАЯ ОБЪЕКТ, ОТВЕТСТВЕННЫЙ ЗА СОЗДАНИЕ ЗАВИСИМОСТИ В СООТВЕТСТВИИ С ЕЁ ТИПОМ (value, constant, factory, service, decorator).
Провайдер доступен через [стандартный сервис $provide](https://docs.angularjs.org/api/auto/service/$provide) (либо косвенно, [через angular.Module - поэтому Вы можете явно с сервисом $provide не взаимодействовать](https://docs.angularjs.org/api/auto/service/$provide):
`Many of these functions are also exposed on angular.Module`).

**Для того, [чтоб имлементируемая Вами зависимость (любого типа, см. ниже) стала доступной, надо для неё реализовать провайдер и зарегистрировать его в сервисе $provide](https://docs.angularjs.org/guide/services#registering-services).**


[$injector использует provider для того, чтоб создать/получить экземпляр зависимости и вернуть его тому, кто эту зависимость попросил](https://docs.angularjs.org/api/auto/service/$provide):
`When you request a service, the $injector is responsible for finding the correct service provider, instantiating it and then calling its $get service factory function to get the instance of the service.`


**FACTORY**


В документации по Angular слово "factory" или "factory function" встречается в разных контекстах, об этом ниже.

Сейчас я только хочу сказать, что независимо от контекста фабрика - это такой [шаблон ООП](https://en.wikipedia.org/wiki/Factory_method_pattern), при использовании которого экземпляры целевых объектов создаются и возвращаются (фабрикуются) некой функцией. Вызывающая сторона остаётся абстрагированной от оператора new и взаиможействия с конструктором конкрутного типа.

**FACTORY for a SERVICE**

[$provide.factory - один из способов создать сервис](https://docs.angularjs.org/api/auto/service/$provide#factory):
`Register a service factory, which will be called to return the service instance.`

[Но сервис можно создать не только с помощью фабрики](https://docs.angularjs.org/api/auto/service/$provide).
Базовый способ один - это $provide.provider. В принципе, можно было бы обойтись только им.
$provide.factory, $provide.service, $provide.value, $provide.constant, $provide.decorator - всё это сокращённые версии (обёртки) $provide.provider.
_Уточнение: $provide.constant не является обёрткой над $provide.provider. Её можно условно таковой считать, но фактически это не так_.

[Почему-то фабрика ($provide.factory) считается самым типичным способом создания сервиса, НО ОН НЕ ЕДИНСТВЕННЫЙ](https://docs.angularjs.org/guide/services#registering-services):
`TYPICALLY you use the Module factory API to register a service`.
В частности, [есть метод $provide.service](https://docs.angularjs.org/api/auto/service/$provide#service), который, к сожалению, может внушить мысль, что он (а, как видите, не factory) - единственный способ создания сервиса.

**Сервис может быть создан любым из выше перечисленных способов.**


**FACTORY for something else**

[С помощью фабрик (не буквально $provide.factory, а фабрик в широком смысле) создаются не только сервисы](https://docs.angularjs.org/guide/di#using-dependency-injection):
`Components such as services, directives, filters, and animations are defined by an injectable factory method or constructor function.`

Например, [для создания директивы используется функция-фабрика](https://docs.angularjs.org/api/ng/provider/$compileProvider#directive).